<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function testCreateUser()
    {
		User::create(
            [
            'name' => 'ali',
            'email' => 'ali.fouly@asda.com',
            'password' => bcrypt('asd'),
            ]
        );

        $this->assertEquals(200, 200);
    }
}
